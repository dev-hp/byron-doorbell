EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:CP C7
U 1 1 5FE8E468
P 2650 7150
F 0 "C7" H 2768 7196 50  0000 L CNN
F 1 "6.8uF 400V" H 2768 7105 50  0000 L CNN
F 2 "" H 2688 7000 50  0001 C CNN
F 3 "~" H 2650 7150 50  0001 C CNN
	1    2650 7150
	1    0    0    -1  
$EndComp
$Comp
L pspice:INDUCTOR L4
U 1 1 5FE8D3D7
P 2250 6850
F 0 "L4" H 2250 7065 50  0000 C CNN
F 1 "2.2mH 10%" H 2250 6974 50  0000 C CNN
F 2 "" H 2250 6850 50  0001 C CNN
F 3 "~" H 2250 6850 50  0001 C CNN
	1    2250 6850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C8
U 1 1 5FE8C784
P 1850 7150
F 0 "C8" H 1968 7196 50  0000 L CNN
F 1 "6.8uF 400V" H 1968 7105 50  0000 L CNN
F 2 "" H 1888 7000 50  0001 C CNN
F 3 "~" H 1850 7150 50  0001 C CNN
	1    1850 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:Varistor VR
U 1 1 5FE83209
P 1100 6650
F 0 "VR" V 842 6650 50  0000 C CNN
F 1 "Varistor" V 933 6650 50  0000 C CNN
F 2 "" V 1030 6650 50  0001 C CNN
F 3 "~" H 1100 6650 50  0001 C CNN
	1    1100 6650
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5FE82259
P 1400 6350
F 0 "R1" H 1330 6304 50  0000 R CNN
F 1 "30R" H 1330 6395 50  0000 R CNN
F 2 "" V 1330 6350 50  0001 C CNN
F 3 "~" H 1400 6350 50  0001 C CNN
	1    1400 6350
	-1   0    0    1   
$EndComp
$Comp
L power:AC #PWR?
U 1 1 5FE805FE
P 1400 6100
F 0 "#PWR?" H 1400 6000 50  0001 C CNN
F 1 "AC" H 1400 6375 50  0000 C CNN
F 2 "" H 1400 6100 50  0001 C CNN
F 3 "" H 1400 6100 50  0001 C CNN
	1    1400 6100
	1    0    0    -1  
$EndComp
$Comp
L power:AC #PWR?
U 1 1 5FE8121B
P 800 6100
F 0 "#PWR?" H 800 6000 50  0001 C CNN
F 1 "AC" H 800 6375 50  0000 C CNN
F 2 "" H 800 6100 50  0001 C CNN
F 3 "" H 800 6100 50  0001 C CNN
	1    800  6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 6650 1400 6650
Wire Wire Line
	800  7150 800  6650
Wire Wire Line
	800  6650 950  6650
Wire Wire Line
	1400 6650 1400 7150
Wire Wire Line
	1100 6850 1850 6850
Wire Wire Line
	1850 7000 1850 6850
Connection ~ 1850 6850
Wire Wire Line
	1850 6850 2000 6850
Wire Wire Line
	2500 6850 2650 6850
Wire Wire Line
	2650 6850 2650 7000
Wire Wire Line
	1850 7550 1850 7300
Wire Wire Line
	1850 7550 2650 7550
Wire Wire Line
	2650 7550 2650 7300
$Comp
L Device:R R10
U 1 1 5FEAC9C6
P 3350 7150
F 0 "R10" H 3420 7196 50  0000 L CNN
F 1 "10M (106)" H 3420 7105 50  0000 L CNN
F 2 "" V 3280 7150 50  0001 C CNN
F 3 "~" H 3350 7150 50  0001 C CNN
	1    3350 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 6850 3350 6850
Wire Wire Line
	3350 6850 3350 7000
Connection ~ 2650 6850
Wire Wire Line
	3350 7550 3350 7300
Connection ~ 2650 7550
$Comp
L Device:D D2
U 1 1 5FEAD370
P 5000 7300
F 0 "D2" V 5046 7220 50  0000 R CNN
F 1 "BYG20J" V 4955 7220 50  0000 R CNN
F 2 "" H 5000 7300 50  0001 C CNN
F 3 "~" H 5000 7300 50  0001 C CNN
	1    5000 7300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1100 7550 1100 7450
Wire Wire Line
	1100 7550 1850 7550
Connection ~ 1850 7550
$Comp
L pspice:INDUCTOR L3
U 1 1 5FEB4F53
P 5000 6600
F 0 "L3" V 4954 6678 50  0000 L CNN
F 1 "XWT" V 5045 6678 50  0000 L CNN
F 2 "" H 5000 6600 50  0001 C CNN
F 3 "~" H 5000 6600 50  0001 C CNN
	1    5000 6600
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 6250 5000 6350
$Comp
L Device:D D3
U 1 1 5FEBBC8E
P 4700 6250
F 0 "D3" H 4700 6033 50  0000 C CNN
F 1 "BYG20J" H 4700 6124 50  0000 C CNN
F 2 "" H 4700 6250 50  0001 C CNN
F 3 "~" H 4700 6250 50  0001 C CNN
	1    4700 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	5000 6250 4850 6250
$Comp
L Device:CP C20
U 1 1 5FEC1FA6
P 5400 6900
F 0 "C20" H 5518 6946 50  0000 L CNN
F 1 "470uF 10V" H 5518 6855 50  0000 L CNN
F 2 "" H 5438 6750 50  0001 C CNN
F 3 "~" H 5400 6900 50  0001 C CNN
	1    5400 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 6250 5400 6750
Connection ~ 5000 6250
Wire Wire Line
	5400 7050 5400 7550
Wire Wire Line
	5400 7550 5000 7550
Connection ~ 5000 7550
$Comp
L Device:R R12
U 1 1 5FEC6343
P 6000 6900
F 0 "R12" H 6070 6946 50  0000 L CNN
F 1 "2K (202)" H 6070 6855 50  0000 L CNN
F 2 "" V 5930 6900 50  0001 C CNN
F 3 "~" H 6000 6900 50  0001 C CNN
	1    6000 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 6750 6000 6250
Wire Wire Line
	6000 6250 5400 6250
Connection ~ 5400 6250
Wire Wire Line
	5400 7550 6000 7550
Wire Wire Line
	6000 7550 6000 7050
Connection ~ 5400 7550
Wire Wire Line
	800  6100 800  6650
Connection ~ 800  6650
Wire Wire Line
	1400 6100 1400 6200
Wire Wire Line
	1400 6500 1400 6650
Connection ~ 1400 6650
Text GLabel 6300 6250 2    50   Input ~ 0
VDD
Wire Wire Line
	6300 6250 6000 6250
Connection ~ 6000 6250
Text GLabel 6000 3650 0    50   Input ~ 0
VDD
Text GLabel 6300 7550 2    50   Input ~ 0
VSS
Text GLabel 6000 5300 0    50   Input ~ 0
VSS
Wire Wire Line
	6000 7550 6300 7550
Connection ~ 6000 7550
$Comp
L Device:C C5
U 1 1 5FECF2BB
P 6550 5000
F 0 "C5" H 6435 4954 50  0000 R CNN
F 1 "C" H 6435 5045 50  0000 R CNN
F 2 "" H 6588 4850 50  0001 C CNN
F 3 "~" H 6550 5000 50  0001 C CNN
	1    6550 5000
	-1   0    0    1   
$EndComp
$Comp
L Device:D D4
U 1 1 5FECF660
P 6350 3650
F 0 "D4" H 6350 3867 50  0000 C CNN
F 1 "TKM7" H 6350 3776 50  0000 C CNN
F 2 "" H 6350 3650 50  0001 C CNN
F 3 "~" H 6350 3650 50  0001 C CNN
	1    6350 3650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6000 3650 6100 3650
Connection ~ 6100 3650
Wire Wire Line
	6100 3650 6200 3650
Wire Wire Line
	6500 3650 6550 3650
Wire Wire Line
	6550 3650 6550 4850
Wire Wire Line
	6000 5300 6100 5300
Wire Wire Line
	6550 5150 6550 5300
Wire Wire Line
	6550 5300 6100 5300
Connection ~ 6100 5300
Wire Wire Line
	8550 5250 8550 5300
Connection ~ 6550 3650
Connection ~ 6550 5300
$Comp
L Device:Speaker SPK
U 1 1 5FEEE109
P 10200 4550
F 0 "SPK" H 10370 4546 50  0000 L CNN
F 1 "8R 0.25W" H 10370 4455 50  0000 L CNN
F 2 "" H 10200 4350 50  0001 C CNN
F 3 "~" H 10190 4500 50  0001 C CNN
	1    10200 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:LED LED1
U 1 1 5FEF3685
P 9800 4150
F 0 "LED1" V 9839 4032 50  0000 R CNN
F 1 "Red" V 9748 4032 50  0000 R CNN
F 2 "" H 9800 4150 50  0001 C CNN
F 3 "~" H 9800 4150 50  0001 C CNN
	1    9800 4150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 5FEF6714
P 9600 4750
F 0 "R2" V 9807 4750 50  0000 C CNN
F 1 "100R (101)" V 9716 4750 50  0000 C CNN
F 2 "" V 9530 4750 50  0001 C CNN
F 3 "~" H 9600 4750 50  0001 C CNN
	1    9600 4750
	0    1    1    0   
$EndComp
Text Label 9150 4650 0    50   ~ 0
SPK-
Text Label 9150 4550 0    50   ~ 0
SPK+
Wire Wire Line
	9150 4650 10000 4650
Wire Wire Line
	10000 4550 9150 4550
Wire Wire Line
	8550 3650 9800 3650
Connection ~ 8550 3650
Wire Wire Line
	9800 4300 9800 4750
Wire Wire Line
	9800 4750 9750 4750
Wire Wire Line
	9450 4750 9150 4750
Wire Wire Line
	5000 6850 5000 7100
$Comp
L Device:C C12
U 1 1 5FEAAC1B
P 4450 6600
F 0 "C12" H 4565 6646 50  0000 L CNN
F 1 "100nF" H 4565 6555 50  0000 L CNN
F 2 "" H 4488 6450 50  0001 C CNN
F 3 "~" H 4450 6600 50  0001 C CNN
	1    4450 6600
	1    0    0    -1  
$EndComp
$Comp
L Power_Management:AUIPS1041R U4
U 1 1 5FEA68E9
P 3950 6250
F 0 "U4" V 4217 6250 50  0000 C CNN
F 1 "10K056" V 4126 6250 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-3_TabPin2" H 3950 6250 50  0001 C CIN
F 3 "https://www.infineon.com/dgdl/Infineon-AUIPS1041-DS-v01_00-EN.pdf?fileId=5546d4625a888733015aae14a0524c63" H 3950 6250 50  0001 C CNN
	1    3950 6250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4250 6250 4450 6250
Wire Wire Line
	4450 6250 4450 6450
Connection ~ 4450 6250
Wire Wire Line
	4450 6250 4550 6250
Connection ~ 3350 7550
Wire Wire Line
	5000 7450 5000 7550
Wire Wire Line
	4350 7100 4450 7100
Connection ~ 5000 7100
Wire Wire Line
	5000 7100 5000 7150
$Comp
L Device:R R11
U 1 1 5FEC4944
P 4200 7100
F 0 "R11" V 3993 7100 50  0000 C CNN
F 1 "1R (1R0)" V 4084 7100 50  0000 C CNN
F 2 "" V 4130 7100 50  0001 C CNN
F 3 "~" H 4200 7100 50  0001 C CNN
	1    4200 7100
	0    1    1    0   
$EndComp
Wire Wire Line
	3950 7100 4050 7100
Wire Wire Line
	3950 6650 3950 7100
Wire Wire Line
	4450 6750 4450 7100
Connection ~ 4450 7100
Wire Wire Line
	4450 7100 5000 7100
$Comp
L Device:D D1
U 1 1 5FED948C
P 3350 6500
F 0 "D1" V 3304 6580 50  0000 L CNN
F 1 "TKM7" V 3395 6580 50  0000 L CNN
F 2 "" H 3350 6500 50  0001 C CNN
F 3 "~" H 3350 6500 50  0001 C CNN
	1    3350 6500
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 6650 3350 6850
Wire Wire Line
	2650 7550 3350 7550
Connection ~ 3350 6850
Wire Wire Line
	3350 6350 3350 6250
Wire Wire Line
	3350 6250 3650 6250
Wire Wire Line
	5000 6250 5400 6250
Wire Wire Line
	3350 7550 5000 7550
$Comp
L Device:D_Bridge_+A-A DB1
U 1 1 5FE857B5
P 1100 7150
F 0 "DB1" V 1146 6806 50  0000 R CNN
F 1 "MB6S" V 1055 6806 50  0000 R CNN
F 2 "" H 1100 7150 50  0001 C CNN
F 3 "~" H 1100 7150 50  0001 C CNN
	1    1100 7150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6550 3650 6950 3650
Wire Wire Line
	6550 5300 6950 5300
$Comp
L Device:CP C19
U 1 1 5FF1C47F
P 6950 5000
F 0 "C19" H 7068 5046 50  0000 L CNN
F 1 "220uF 10V" H 7068 4955 50  0000 L CNN
F 2 "" H 6988 4850 50  0001 C CNN
F 3 "~" H 6950 5000 50  0001 C CNN
	1    6950 5000
	1    0    0    -1  
$EndComp
Connection ~ 6950 3650
Wire Wire Line
	6950 3650 6950 4750
Wire Wire Line
	6950 5150 6950 5300
Wire Wire Line
	6950 5300 8550 5300
$Comp
L Device:C C21
U 1 1 5FECE13E
P 6100 5000
F 0 "C21" H 5985 4954 50  0000 R CNN
F 1 "C" H 5985 5045 50  0000 R CNN
F 2 "" H 6138 4850 50  0001 C CNN
F 3 "~" H 6100 5000 50  0001 C CNN
	1    6100 5000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6100 5300 6100 5150
$Comp
L MCU_Microchip_PIC12:PIC12F508-IMC U3
U 1 1 5FED410E
P 8550 4650
F 0 "U3" H 8550 5431 50  0000 C CNN
F 1 "F10-38(AC) 200410" H 8550 5340 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 9150 5300 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/41236E.pdf" H 8550 4650 50  0001 C CNN
	1    8550 4650
	-1   0    0    -1  
$EndComp
Connection ~ 6950 5300
Wire Wire Line
	9800 3650 9800 4000
Wire Wire Line
	8550 3650 8550 4050
Wire Wire Line
	7700 4750 7950 4750
Wire Wire Line
	6100 3650 6100 4250
Connection ~ 6100 4250
Wire Wire Line
	6100 4250 6100 4850
$Comp
L RF:CMT2220LY U1
U 1 1 5FF8091B
P 3300 2000
F 0 "U1" H 3300 2781 50  0000 C CNN
F 1 "YD1207 6839BFH402 (CMT2220LY)" H 3300 2690 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 3900 2650 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/41236E.pdf" H 3300 2000 50  0001 C CNN
	1    3300 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:Antenna ANT
U 1 1 5FF82FD3
P 1450 850
F 0 "ANT" H 1530 793 50  0000 L CNN
F 1 "Antenna" H 1530 748 50  0001 L CNN
F 2 "" H 1450 850 50  0001 C CNN
F 3 "~" H 1450 850 50  0001 C CNN
	1    1450 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 5FF87553
P 1250 2000
F 0 "L1" H 1303 2046 50  0000 L CNN
F 1 "27nH" H 1303 1955 50  0000 L CNN
F 2 "" H 1250 2000 50  0001 C CNN
F 3 "~" H 1250 2000 50  0001 C CNN
	1    1250 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5FF889FE
P 1600 2000
F 0 "C1" H 1715 2046 50  0000 L CNN
F 1 "3pF" H 1715 1955 50  0000 L CNN
F 2 "" H 1638 1850 50  0001 C CNN
F 3 "~" H 1600 2000 50  0001 C CNN
	1    1600 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1850 1450 1850
Wire Wire Line
	1450 1050 1450 1650
Connection ~ 1450 1850
Wire Wire Line
	1450 1850 1600 1850
Text GLabel 7000 3050 2    50   Input ~ 0
VSS
Wire Wire Line
	1250 2150 1450 2150
Wire Wire Line
	1450 2150 1450 2350
Connection ~ 1450 2150
Wire Wire Line
	1450 2150 1600 2150
$Comp
L Device:C C2
U 1 1 5FF9653A
P 1700 1650
F 0 "C2" V 1448 1650 50  0000 C CNN
F 1 "2.7pF" V 1539 1650 50  0000 C CNN
F 2 "" H 1738 1500 50  0001 C CNN
F 3 "~" H 1700 1650 50  0001 C CNN
	1    1700 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	1550 1650 1450 1650
Connection ~ 1450 1650
Wire Wire Line
	1450 1650 1450 1850
$Comp
L Device:L L2
U 1 1 5FF9E08E
P 1900 2000
F 0 "L2" H 1953 2046 50  0000 L CNN
F 1 "39nH" H 1953 1955 50  0000 L CNN
F 2 "" H 1900 2000 50  0001 C CNN
F 3 "~" H 1900 2000 50  0001 C CNN
	1    1900 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 1850 1900 1650
Wire Wire Line
	1900 1650 1850 1650
Wire Wire Line
	1900 2150 1900 2350
$Comp
L Device:C C9
U 1 1 5FFAD980
P 2250 1150
F 0 "C9" H 2365 1196 50  0000 L CNN
F 1 "100nF" H 2365 1105 50  0000 L CNN
F 2 "" H 2288 1000 50  0001 C CNN
F 3 "~" H 2250 1150 50  0001 C CNN
	1    2250 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1300 2250 2000
$Comp
L Device:C C6
U 1 1 5FFCCEAE
P 4400 2650
F 0 "C6" H 4515 2696 50  0000 L CNN
F 1 "470nF" H 4515 2605 50  0000 L CNN
F 2 "" H 4438 2500 50  0001 C CNN
F 3 "~" H 4400 2650 50  0001 C CNN
	1    4400 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5FFCDCEA
P 4000 2650
F 0 "C4" H 4115 2696 50  0000 L CNN
F 1 "1uF" H 4115 2605 50  0000 L CNN
F 2 "" H 4038 2500 50  0001 C CNN
F 3 "~" H 4000 2650 50  0001 C CNN
	1    4000 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3050 4400 3050
Connection ~ 4000 3050
Wire Wire Line
	1900 2350 1450 2350
Connection ~ 1450 2350
Wire Wire Line
	7900 4550 7950 4550
Text Label 7700 1900 0    50   ~ 0
PLAY
Wire Wire Line
	7250 1900 7900 1900
Wire Wire Line
	7900 1900 7900 4550
Wire Wire Line
	7400 2650 7400 2000
Wire Wire Line
	7400 2000 7250 2000
$Comp
L Device:R R4
U 1 1 6002ED0B
P 5100 1350
F 0 "R4" H 5170 1396 50  0000 L CNN
F 1 "10K (103)" H 5170 1305 50  0000 L CNN
F 2 "" V 5030 1350 50  0001 C CNN
F 3 "~" H 5100 1350 50  0001 C CNN
	1    5100 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2000 5100 2000
Wire Wire Line
	5100 2000 5100 1500
Wire Wire Line
	5100 1200 5100 750 
Wire Wire Line
	5100 750  5750 750 
Wire Wire Line
	5750 1200 5750 750 
Wire Wire Line
	4400 3050 6650 3050
Wire Wire Line
	6650 3050 6650 2600
Connection ~ 4400 3050
Connection ~ 5100 750 
Wire Wire Line
	6650 750  5750 750 
Wire Wire Line
	6650 750  6650 1400
Connection ~ 5750 750 
$Comp
L Device:R R28
U 1 1 6002F726
P 5750 1350
F 0 "R28" H 5820 1396 50  0000 L CNN
F 1 "47K (473)" H 5820 1305 50  0000 L CNN
F 2 "" V 5680 1350 50  0001 C CNN
F 3 "~" H 5750 1350 50  0001 C CNN
	1    5750 1350
	1    0    0    -1  
$EndComp
$Comp
L MCU_Microchip_PIC12:PIC12F508-IMC U2
U 1 1 5FFF2C62
P 6650 2000
F 0 "U2" H 6650 2781 50  0000 C CNN
F 1 "MCU" H 6650 2690 50  0001 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 7250 2650 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/41236E.pdf" H 6650 2000 50  0001 C CNN
	1    6650 2000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6050 1900 5750 1900
Wire Wire Line
	5750 1900 5750 1500
$Comp
L Device:Crystal Y1
U 1 1 600CD709
P 2500 2650
F 0 "Y1" V 2546 2519 50  0000 R CNN
F 1 "JMR13.52127M" V 2455 2519 50  0000 R CNN
F 2 "" H 2500 2650 50  0001 C CNN
F 3 "~" H 2500 2650 50  0001 C CNN
	1    2500 2650
	0    -1   1    0   
$EndComp
Wire Wire Line
	1450 2350 1450 3050
Wire Wire Line
	4000 2800 4000 3050
Wire Wire Line
	4400 2800 4400 3050
Wire Wire Line
	4000 2100 4000 2500
Wire Wire Line
	4400 2500 4400 2000
Text Label 5100 2650 0    50   ~ 0
DOUT
Wire Wire Line
	4850 2650 7400 2650
Wire Wire Line
	1450 3050 2250 3050
Wire Wire Line
	2250 750  2250 1000
Wire Wire Line
	2250 750  3300 750 
Connection ~ 2250 2000
Wire Wire Line
	2250 2000 2250 3050
Connection ~ 2250 3050
Wire Wire Line
	2250 3050 2500 3050
Wire Wire Line
	3300 750  3300 1400
Connection ~ 3300 750 
Wire Wire Line
	3300 750  5100 750 
Wire Wire Line
	3300 2600 3300 3050
Connection ~ 3300 3050
Wire Wire Line
	3300 3050 4000 3050
Wire Wire Line
	2500 2100 2500 2500
Wire Wire Line
	2500 2100 2700 2100
Wire Wire Line
	2500 2800 2500 3050
Connection ~ 2500 3050
Wire Wire Line
	2500 3050 3300 3050
Wire Wire Line
	2250 2000 2700 2000
Wire Wire Line
	1900 1650 2500 1650
Wire Wire Line
	2500 1900 2700 1900
Connection ~ 1900 1650
Wire Wire Line
	3900 2100 4000 2100
Wire Wire Line
	4400 2000 3900 2000
Wire Wire Line
	3900 1900 4850 1900
Wire Wire Line
	4850 1900 4850 2650
Wire Wire Line
	2500 1650 2500 1900
$Comp
L Switch:SW_DIP_x01 Melody
U 1 1 5FF35766
P 7400 4750
F 0 "Melody" H 7400 4925 50  0000 C CNN
F 1 "SW_DIP_x01" H 7400 4926 50  0001 C CNN
F 2 "" H 7400 4750 50  0001 C CNN
F 3 "~" H 7400 4750 50  0001 C CNN
	1    7400 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 4250 6100 4250
$Comp
L Switch:SW_DIP_x01 Volume
U 1 1 5FF325BE
P 7400 4250
F 0 "Volume" H 7400 4425 50  0000 C CNN
F 1 "SW_DIP_x01" H 7400 4426 50  0001 C CNN
F 2 "" H 7400 4250 50  0001 C CNN
F 3 "~" H 7400 4250 50  0001 C CNN
	1    7400 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3650 8550 3650
Wire Wire Line
	7100 4750 6950 4750
Connection ~ 6950 4750
Wire Wire Line
	6950 4750 6950 4850
Wire Wire Line
	7700 4250 7700 4650
Wire Wire Line
	7700 4650 7950 4650
Wire Wire Line
	6650 750  8550 750 
Wire Wire Line
	8550 750  8550 3650
Connection ~ 6650 750 
Wire Wire Line
	7000 3050 6650 3050
Connection ~ 6650 3050
$EndSCHEMATC
