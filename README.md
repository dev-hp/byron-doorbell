# Byron Wireless Doorbell Extension

## Overview

When you ring our doorbell, you will hear the sound of an old-fashioned [electric bell](https://en.wikipedia.org/wiki/Electric_bell). Its simple circuit consists just of an 8V/1A transformer, a switch, a doorbell and some wire.

It has a really nice sound, we love it. Unfortunately, as it is a fairly old device, it sometimes malfunctions. Additionally, we can not hear it when we are in the garden or when the kitchen extractor fan is on 'high'.

So... we need some sort of 'backup'. A bell that rings in parallel to the existing one, and that can also be mounted a couple of meters away. And we don't want to have the thing to make a loud ring in the garden at all times, so maybe it could be something portable?

Hmmm... how about an inexpensive wireless doorbell? Before checking the specialist DIY shops, let's see what the local shops, like Action, Big Bazar or Kruidvat have on offer. These often have some gadgets or household items, and if they have what you need, it's ususally not very expensive.

Well, [Action had an inexpensive wireless doorbell set](https://www.action.com/nl-nl/p/draadloze-deurbel/) - for about 6 EUR, you get a remote doorbell button (a wireless transmitter) and a plug-in doorbell receiver unit. The CR2032 battery for the transmitter is included, the other unit just plugs into a normal AC power outlet (=wall socket). Wow!

This project started because it operates in the ISM (Industial, Scientific and Medical) radio band, at 433.92MHz. Because I happen to have [some experience with receiving data in this band](https://gitlab.com/hp-uno/uno_log_433), and also [dabbled a bit in PIC designs](https://gitlab.com/dev-hp/pyramid-mood-light), I got the tools and parts to make a secondary 433MHz transmitter for it. (Or receive its data at a second place. Or log it.)

This is the project of a secondary 433.92MHz transmitter of a doorbell signal to a wireless doorbell receiver.

## The Original Doorbell Device

Name: `Byron DBY-23712W Wireless doorbell set`

- TX DBY-23710W|Push, Battery CR2032 3V= 30mA, 433.92MHz, Batch No.T94255, IP44  
  smartwares Europe, www.chbyron.eu
- RX DBY-23712W Indoor chime, 433.92MHz, 50mA, Batch No.T94255 (220V EU mains plug)  
  Smartwares Europe, Jules Verneweg 97, 5015 BH Tilburg, The Netherlands
- Purchased 2020-11-27, EUR5.99, from 'Action' in Utrecht (Hoog Catharijne)

Links to the components:

* [Transmitter](doorbell-tx.md)
* [Receiver](doorbell-rx.md)
