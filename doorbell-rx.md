# Doorbell-RX

## The Original Doorbell Receiver

Name: `Byron DBY-23712W Wireless doorbell set`

- RX DBY-23712W Indoor chime, 433.92MHz, 50mA, Batch No.T94255 (220V EU mains plug)  
  Smartwares Europe, Jules Verneweg 97, 5015 BH Tilburg, The Netherlands

## Operation

The doorbell receiver plugs into a 220V (240V) mains outlet. Visible features on the front are 

- a red LED that flashes when the doorbell rings
- a loudspeaker grille
- one pushbutton for volume (three volume settings and 'off')
- one pushbutton to cycle through 38 different melodies

## PCB images

### Top view of the single-sided receiver PCB

![Byron Wireless Doorbell Receiver - PCB Top View](image/dby-23712w-t.jpg)

### Mirrored top view of the single-sided PCB (for analysis)

![Byron Wireless Doorbell Receiver - PCB Top View, mirrored for analysis](image/dby-23712w-t-rev.jpg)

### Bottom view of the single-sided receiver PCB

![Byron Wireless Doorbell Receiver - PCB Bottom View](image/dby-23712w-b.jpg)

## Schematic

Please note that there might still be errors in the schematic. Pin names are mostly wrong, because I did not find much documentation about these ICs.

### Power supply part

The power supply is not a simple 'capacitive dropper', but centers around U4, an SOT23-3 IC.
To me this looks like a buck converter. The output voltage is ca. 5V.

- Luckily I bought a second 'tinker' unit, for analysis, and to have a second receiver unit upstairs in the study.
- Unfortunately, I short-circuited the bridge rectifier MB6S when I tried to measure the input and output voltage of the power supply part (grmbl!).
- For a few weeks (waiting for a replacement part), I connected a 3V two-AA-cell battery supply to a contact behind the power supply.
- This was enough to run the doorbell, but when the battery voltage dropped too low, the bell would not stop ringing anymore. :-)
- Looking on the 'net, I found that Byron also makes a battery-only variant of the receiver. A nonstop ringing doorbell would make for a good 'low-battery' indicator (very annoying, too). So this behaviour is probably by design.

![Power supply part](hardware/doorbell-rx-sch-power.png)

### Main part (receiver IC, controller, sound playback)

The doorbell playback chip does not 'remember' the chosen tune: if disconnected from power and reconnected, it will 'ring' the first 'ding-dong' tune on the highest volume setting.

The receiver 'remembers' the transmitter code, it can be coupled and uncoupled by receiving slightly different datagrams from a transmitter.

Standby current is ca. 7mA, playback current goes up to ca. 80mA. On the highest volume setting it is surprisingly loud.

To me this looks like a straightforward circuit:

- U1, a 433MHz-receiver (from the pinout this is a CMT2220LY or equivalent) that receives and demodulates the On-Off-Keying signal (OOK PWM, see transmitter)
- U2, an unnamed microcontroller which analyzes the incoming the signal and - if a match is detected - triggers the doorbell playback
- U3, another microcontroller (or a dedicated doorbell chip?) - which 'wakes up' when a PLAY signal comes in (or if one of the Melody or Volume buttons is pressed), and plays one of 38 tunes

![Main part](hardware/doorbell-rx-sch-main.png)

