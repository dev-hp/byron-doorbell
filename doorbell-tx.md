# Doorbell-TX

## The Original Doorbell Transmitter

Name: `Byron DBY-23712W Wireless doorbell set`

- TX DBY-23710W|Push, Battery CR2032 3V= 30mA, 433.92MHz, Batch No.T94255, IP44  
  smartwares Europe, www.chbyron.eu

### Summary of the Original Sender

At first I wanted to use the existing doorbell 'pushbutton' transmitter, and just modify it to send a 'push' signal when somebody presses the outside bell button.

The sender is labeled DBY-23710W, and it:

- runs on a 3V battery (CR2032), so we need a sort of power supply to transform the existing 8V/1A feed to 3V
- has a pushbutton switch 'S1' as 'bell button'
- the pushbutton is lit by three red LEDs when the device transmits the signal, so the person on the outside gets visible feedback (and can see when the battery is empty)
- has a pushbutton switch 'S2' on the inside (where the battery is) to un-pair the switch from the doorbell receiver
- comes in an IP44 case, with a compartment for a name tag

### DBY-23710W PCB Top view

![Top view of the circuit board (this is what you see when you insert the battery)](image/dby-23710w-t.jpg)

### DBY-23710W PCB Bottom view (doorbell button side)

![Bottom view of the circuit board (the side of the outer push button)](image/dby-23710w-b.jpg)

Missing: Schematic of the original transmitter circuit.

- U2, the SOT23 6-pin chip that transmits the 433.920MHz signal reads 'BO3668 54J9'. Maybe a [MICRF113](https://ww1.microchip.com/downloads/en/DeviceDoc/micrf113.pdf) compatible chip?
- U1, the 8-pin chip, seems to be an 'unnamed microcontroller' (cf. Padauk et al.)

### Modification or Replacement?

OK. We can probably shorten (bridge) switch S1, add a small power supply (from 8V to 3V), mount it, and we are done.

Hmm... I like the design of the sender, so I would like to keep it intact. And I have a stash of PIC microcontrollers and TX433 modules lying around. With the experience I gained from [analyzing the NOR-TEC weather station data](https://gitlab.com/hp-uno/uno_log_433), maybe I can make a transmitter? What do we need to make one?

Let's make a design!

## Hardware

### Requirements

The replacement sender needs:

- an inexpensive microcontroller that has at least one output pin
- a 433.92MHz transmitter module
- a power supply from 8V/1A to whatever voltage we need for the microcontroller and for the transmitter module

### Component Selection

Looking at the parts I have used in the past, and at the ones I have at home:

- An Arduino would work, but would be 'overkill' for a simple transmitter. Also an Arduino is quite expensive (ca. 5 - 10 EUR) compared to the whole doorbell combo (6 EUR!!).
- From my stash of Microchip PIC controllers, I chose the [PIC16F18313](https://www.microchip.com/wwwproducts/en/PIC16F18313):
  - the list price for this device is about 0.75 EUR
  - it comes in an 8 pin DIP package
  - it operates between 2.3 and 5.5V
- From my selection of 433.93MHz transmitter modules, I chose the [FS1000A](https://www.componentsinfo.com/fs1000a-433mhz-rf-transmitter-xy-mk-5v-receiver-module-explanation-pinout/)
  - the list price for this is around 1 EUR
  - it operates between 3V and 12V (nicely, the voltage range for 433MHz transmitters starts around 3V, much lower than the range for most of the 433MHz receivers which artar around 5V)
- For the power supply
  - we use a [simple voltage regulator controlled by a zener diode](https://www.powerelectronicsnews.com/power-supply-design-notes-zener-diode-voltage-regulator/),
    actually I remembered the circuit I analyzed for [the Conic TG-621 repair project](https://gitlab.com/repair1/conic-tg-621)
  - the usable voltage range is between 3V and 5.5V (limited by the PIC)
  - the PIC and the 433MHz TX module together need less than 10mA, so heat dissipation will not be a problem
  - from the ones I have at home, I went for a 5.1V zener diode and an unmarked NPN transistor which I had lying around (probably came in a mixed bag from [Pollin](https://www.pollin.de/))
  - this zener/transistor combination results in an output voltage of ca. 4.5V
  - as buffer capacitor, I use a 470uF/10V model (which I also found in my capacitor box)

### Schematics

![Replacement doorbell transmitter schematics](hardware/doorbell-tx-sch.png)

### Stripboard Diagram

![Replacement doorbell transmitter schematics](hardware/doorbell-tx-strip.png)

### Stripboard Photos

![Replacement doorbell transmitter Photo 1](hardware/doorbell-tx-img1.jpg)

![Replacement doorbell transmitter Photo 2](hardware/doorbell-tx-img2.jpg)

### Hardware Implementation Notes

- The current circuit and stripboard design leaves room for extension.
  - All unused PINs are available
  - They are not connected to GND (also not MCLR), but this seems not problematic
- Measuring the current at 5V external (USB powerbank-powered)
  - the PIC required ca. 900uA when active
  - the power supply (R1/ZD1) and FS1000A standby current was ca. 2mA
  - the maximum current (transmitting) was ca. 8 mA
- The 470uF electrolytic capacitor was a random value. Maybe I add a 2200uF capacitor later to check how much that extends the transmission time.

## Analysis and Development

For my [Arduino data logger for the Nor-tec weather station + sensor 73383](https://gitlab.com/hp-uno/uno_log_433),
I had already learned how to use an RTL-SDR stick and [rtl_433](https://github.com/merbanan/rtl_433) to receive datagrams from 433MHz senders.

So I plugged my 'DVB-T+FM-DAB 820T2 & SDR' stick into a free USB port, started rtl_433, and pressed the doorbell switch (S1).

### rtl_433 log analysis

This is what I learned from pressing the 'door bell' button (S1):

- rtl_433 reports a reading from 'Smoke detector GS 558' when I press the button
- every button press keeps the sender on for ca. 2 seconds
- the sender transmits 20 datagrams with 33 bits each
- the wave form is on-off-keying (OOK) with pulse-width-modulation (PWM)
- between datagrams is a gap of ca. 7ms
- every other button press the code changes
- it alternates between two fixed (sender-specific?) different bit patterns

This is what I learned from pressing the 'delete link' button (S2):

- the format is the same, but with two different bit patterns

Other 'lessons learned':

- if the Automatic Gain Control of the RTL-SDR stick is ON, I get a lot of noise at the start of the reception
- setting the gain to a fixed, low setting (e.g. '10' in the config file = 1db) results in much nicer sample files
- Audacity can import the sample files (\*.cu8). This makes it much easier to look at the waveforms and data!

Here is a summary of the protocol:

| Duration | Description                                         |
| -------: | --------------------------------------------------- |
|    520us | length 0-bit signal                                 |
|   1544us | length 0-bit gap                                    |
|   1556us | length 1-bit signal                                 |
|    516us | length 1-bit gap                                    |
|    520us | length Inter-packet signal (0-bit)                  |
|   7244us | length Inter-packet gap (includes 1544us 0-bit gap) |
|   2072us | Bit period                                          |

And when you press the door bell button for a really long time? There is a little surprise: an inter-packet gap is inserted BOTH before the inter-packet signal AND after the inter-packet signal pulse.

### Datagram image

This image is part of a screenshot of Audacity, after import of several \*.cu8 files saved with rtl_433:

![Datagram image from Audacity Raw Data import](image/datagram-tx.png)

In the middle of the lower half you can see the special inter-packet gap structure that notifies the receiver of the condition 'button still pressed'.

### Software Implementation Notes

A few assumptions/decisions had to be made for the analysis:

1. a signal is sent when the 433.92MHz transmitter in 'on' (transmitting)
2. a gap is sent when the transmitter is 'off' (silent)
3. a long signal is '1', and a short signal is '0'
4. the most significant bit is transmitted first

And here are a few notes about the implementation:

- The BELL_1 and BELL_2 constants in the published source code were obtained from my specific Byron Wireless doorbell set transmitter. As each transmitter unit probably comes with a different code, you will need to change it. (In order to not lose the connection between server and receiver, the code is fixed and survives inserting a new battery.)
- Luckily the receiver does not require reception of alternating patterns to 'ring'.
- This made the design much easier, because we don't need to store (in EEPROM) which pattern was sent last. (Also we don't know what/if the other sender transmitted something - there could be multiple!)
- The routine that sends the data packets is quite simplistic, and it is so short that I kept everything in `main()`.
- An operating frequency of 4 MHz looked like a good balance between accuracy and size of the required constants.
- Because the transmitter is only turned on when someone pushes the door bell button
  - we can't have anything in RAM 'persistent' between botton presses
  - we don't need to sample any of the inputs, just transmit the Wireless Datagram at power-on
  - the transmitter uses very little power, so we don't need any sleep function(s)

Open issues:

- Setting the oscillator to the proper frequency was a struggle - it just did not seem to work properly, so in the end I kept the main oscillator at 32MHz and divided by 8 to get the 4 MHz I wanted.
- Meanwhile I read up about the oscillator and found more examples. I plan updating the code with some better initialization, to use 'real' 4MHz.
- It would be nice to know more about the data pattern, so if anyone has more information, [please contact me](mailto:hpatzke@gmx.net).

### Software

MPLAB IDE for development and MPLAB IPE for programming are the standard tools for Microchip PIC controllers. I had used them already for the [Pyramid Mood Light](https://gitlab.com/dev-hp/pyramid-mood-light) project, so there was nothing new to install.

Here is the core of the `main()` function (the full source code, with all configuration pragma statements, is in the repository):

```text
[...lines_omitted...]

// Doorbell pattern for the Byron Wireless Doorbell Set from Action
#define BELL_1  0x01234567UL
#define BELL_2  0x76543210UL
// Number of packets sent
#define REPEATS 20
// Signal and Gap length for 0-bit
#define BT0_SIG 524
#define BT0_GAP 1544
// Signal and Gap length for 1-bit
#define BT1_SIG 1560
#define BT1_GAP 512
// Signal and Gap length for SYNC
#define SYN_SIG 524
#define SYN_GAP 7244

unsigned long pattern;
char rpt, pos;

// Tell the compiler the working frequency (needed for _delay functions)
#define _XTAL_FREQ 4000000

#define OUT_SIG 0b00100000 // RA5, pin 2
#define OUT_GAP 0b00000000 // all low

int main(void) {
    [...lines_omitted...]
    LATA    = OUT_GAP;    // all output latches low


    for (rpt = REPEATS; rpt; --rpt) {

        pattern = BELL_1;

        for (pos = 32; pos; --pos) {
            if (pattern & (1UL << 31)) {
                LATA = OUT_SIG;
                __delay_us(BT1_SIG);
                LATA = OUT_GAP;
                __delay_us(BT1_GAP);
            } else {
                LATA = OUT_SIG;
                __delay_us(BT0_SIG);
                LATA = OUT_GAP;
                __delay_us(BT0_GAP);
            }
            // next bit in pattern
            pattern <<= 1;
        }

        // SYNC BIT
        LATA = OUT_SIG;
        __delay_us(SYN_SIG);
        LATA = OUT_GAP;
        __delay_us(SYN_GAP);
    }

    return 0;
}
```

## Connected in the real world

After connecting the circuit to the real bell transformer, in parallel to the existing doorbell, this is how the waveform looks like:

![Datagram image zoomed-out](image/datagram-tx-50hz-a.png)

Yes, the bell transformer delivers 8V/1A Alternating Current.

- With a 10mA load, the 470uF capacitor smoothes the pulsating DC after the diode with a residual 50Hz overlay of about 0.5V (at least I hope my computation is correct)
- A more exact value is dU = 2 \* (I \* dt) / C = 2 \* (0.01A \* 0.01s) / 0.00047F = ca. 0.4255V
- This is a bit less than 10% of the nominal operating voltage (4.5V)

When we zoom in, however, it is hardly noticable:

![Datagram image zoomed-out](image/datagram-tx-50hz-b.png)

As it does not impact the performance (the Byron doorbell receiver rings nicely in the kitchen), I'll leave it for now.

### Update

After a couple of days, the doorbell did not ring in the kitchen anymore. The circuit itself did not seem to be broken, but I suspected the 50Hz ripple, or interference from the electric doorbell (in parallel) to have some effect. 

Therefore my first approach was to stabilize the power voltage. I added another mini-stripboard piece with just a bridge rectifier and a 2200uF/35V electrolytic capacitor between the 8V/1A feed and the power input of the transmitter. This seems to have nicely fixed the problem.

## Appendix

### rtl_433 raw log data

Just to give an impression how rtl_433 analyzes the sampled data, here are a few output snippets.
(During the analysis, comparing the raw sampled data with the rtl_433 output, I added comments.)

```text
g003_433.92M_250k.cu8
 Test mode active. Reading samples from file: g003_433.92M_250k.cu8
Detected OOK package	@0.011352s
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
time      : @0.011352s
model     : Smoke detector GS 558                  id        : 3813
unit      : 25           learn     : 0             Raw Code  : a1dcb9
Analyzing pulses...
Total count:  626,  width: 1400.18 ms		(350044 S)
Pulse width distribution:
 [ 0] count:  342,  width: 1556 us [1544;1584]	( 389 S)
 [ 1] count:  284,  width:  520 us [508;552]	( 130 S)
Gap width distribution:
 [ 0] count:  341,  width:  516 us [496;536]	( 129 S)
 [ 1] count:  266,  width: 1544 us [1516;1560]	( 386 S)
 [ 2] count:   18,  width: 7244 us [7224;7260]	(1811 S)
Pulse period distribution:
 [ 0] count:  607,  width: 2072 us [2056;2096]	( 518 S)
 [ 1] count:   18,  width: 7764 us [7752;7784]	(1941 S)
Level estimates [high, low]:  16266,   7255
RSSI: -0.0 dB SNR: 3.5 dB Noise: -3.5 dB
Frequency offsets [F1, F2]:    1495,      0	(+5.7 kHz, +0.0 kHz)
Guessing modulation: Pulse Width Modulation with multiple packets
Attempting demodulation... short_width: 520, long_width: 1556, reset_limit: 7264, sync_width: 0
Use a flex decoder with -X 'n=name,m=OOK_PWM,s=520,l=1556,r=7264,g=1564,t=412,y=0'
pulse_demod_pwm(): Analyzer Device
bitbuffer:: Number of rows: 19
[00] {33} 62 c4 7a 2c 80 :

011000101100010001111010001011001
100111010011101110000101110100110
------
Bit pattern as it is transmitted:
1001 1101 0011 1011 1000 0101 1101 0011 0

{33} 9d 3b 85 d3 00

7244us - Inter-packet gap (includes 1544us 0-bit gap from previous packet)
5700us - Inter-packet gap net length
7244us - Inter-packet gap
2072us - Bit period
 520us - length 0-bit signal
1544us - length 0-bit gap
1556us - length 1-bit signal
 516us - length 1-bit gap

gap        1   0   0   1   1   1   0   1   0   0   1   1
-----------OOO-O---O---OOO-OOO-OOO-O---OOO-O---O---OOO-OOO-

1   0   1   1   1   0   0   0   0   1   0   1   1   1   0   1
OOO-O---OOO-OOO-OOO-O---O---O---O---OOO-O---OOO-OOO-OOO-O---OOO-

0   0   1   1   0
O---O---OOO-OOO-O---

===============================================================================

g004_433.92M_250k.cu8
Test mode active. Reading samples from file: g004_433.92M_250k.cu8
Detected OOK package	@0.278628s
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
time      : @0.278628s
model     : Smoke detector GS 558                  id        : 24400
unit      : 12           learn     : 0             Raw Code  : abea0c
Analyzing pulses...
Total count:  659,  width: 1474.92 ms		(368730 S)
Pulse width distribution:
 [ 0] count:  339,  width:  524 us [512;556]	( 131 S)
 [ 1] count:  320,  width: 1560 us [1552;1592]	( 390 S)
Gap width distribution:
 [ 0] count:  320,  width: 1544 us [1516;1560]	( 386 S)
 [ 1] count:  319,  width:  512 us [488;528]	( 128 S)
 [ 2] count:   19,  width: 7244 us [7220;7256]	(1811 S)
Pulse period distribution:
 [ 0] count:  639,  width: 2072 us [2060;2096]	( 518 S)
 [ 1] count:   19,  width: 7768 us [7760;7780]	(1942 S)
Level estimates [high, low]:  16205,   6400
RSSI: -0.0 dB SNR: 4.0 dB Noise: -4.1 dB
Frequency offsets [F1, F2]:    1128,      0	(+4.3 kHz, +0.0 kHz)
Guessing modulation: Pulse Width Modulation with multiple packets
Attempting demodulation... short_width: 524, long_width: 1560, reset_limit: 7260, sync_width: 0
Use a flex decoder with -X 'n=name,m=OOK_PWM,s=524,l=1560,r=7260,g=1564,t=412,y=0'
pulse_demod_pwm(): Analyzer Device
bitbuffer:: Number of rows: 20
[00] {33} cf a8 2a 6c 80 : 11001111 10101000 00101010 01101100 1

110011111010100000101010011011001
001100000101011111010101100100110
------
Bit pattern as it is transmitted:
(assumes long_signal_short_gap = 1 and short_signal_log_gap = 0)
0011 0000 0101 0111 1101 0101 1001 0011 0

{33} 30 57 d5 93 00


============================================================
Pressing S2 (un-pair button)


Test mode active. Reading samples from file: g018_433.92M_250k.cu8
Detected OOK package	@0.818100s
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
time      : @0.818100s
model     : Smoke detector GS 558                  id        : 23118
unit      : 14           learn     : 0             Raw Code  : cb49ce
Analyzing pulses...

Bit pattern
0111 0011 1001 0010 1101 0011 1001 0011 0
{33} 73 92 d3 93 00

-----
Test mode active. Reading samples from file: g019_433.92M_250k.cu8
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
time      : @0.269464s
model     : Smoke detector GS 558                  id        : 3139
unit      : 7            learn     : 0             Raw Code  : c18867
*** signal_start = 60086, signal_end = 951657, signal_len = 891571, pulses_found = 1513

Bit pattern
1110 0110 0001 0001 1000 0011 1101 0011 0
{33} e6 11 83 d3 00

///////////////////////////cut file///////////////////////////////////////////

Test mode active. Reading samples from file: g018_433.92M_250k_cut.cu8.aiff
Detected OOK package    @0.452756s
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
time      : @0.452756s
model     : Smoke detector GS 558                  id        : 23118
unit      : 14           learn     : 0             Raw Code  : cb49ce
Analyzing pulses...
Total count:  262,  width: 588.69 ms            (147173 S)
Pulse width distribution:
 [ 0] count:  135,  width: 1576 us [1568;1592]  ( 394 S)
 [ 1] count:  127,  width:  524 us [520;536]    ( 131 S)
Gap width distribution:
 [ 0] count:  135,  width:  520 us [516;540]    ( 130 S)
 [ 1] count:  119,  width: 1564 us [1560;1580]  ( 391 S)
 [ 2] count:    7,  width: 7316 us [7316;7320]  (1829 S)
Pulse period distribution:
 [ 0] count:  254,  width: 2096 us [2080;2120]  ( 524 S)
 [ 1] count:    7,  width: 7840 us [7840;7844]  (1960 S)
Level estimates [high, low]:  16259,   8073
RSSI: -0.0 dB SNR: 3.0 dB Noise: -3.1 dB
Frequency offsets [F1, F2]:    1424,      0     (+5.4 kHz, +0.0 kHz)
Guessing modulation: Pulse Width Modulation with multiple packets
Attempting demodulation... short_width: 524, long_width: 1576, reset_limit: 7324, sync_width: 0
Use a flex decoder with -X 'n=name,m=OOK_PWM,s=524,l=1576,r=7324,g=1584,t=420,y=0'
pulse_demod_pwm(): Analyzer Device
bitbuffer:: Number of rows: 8
[00] {31} 31 b4 b1 b2    : 00110001 10110100 10110001 1011001
[01] {33} 8c 6d 2c 6c 80 : 10001100 01101101 00101100 01101100 1
[02] {33} 8c 6d 2c 6c 80 : 10001100 01101101 00101100 01101100 1
[03] {33} 8c 6d 2c 6c 80 : 10001100 01101101 00101100 01101100 1
[04] {33} 8c 6d 2c 6c 80 : 10001100 01101101 00101100 01101100 1
[05] {33} 8c 6d 2c 6c 80 : 10001100 01101101 00101100 01101100 1
[06] {33} 8c 6d 2c 6c 80 : 10001100 01101101 00101100 01101100 1
[07] {33} 8c 6d 2c 6c 80 : 10001100 01101101 00101100 01101100 1


D:\App\rtl-sdr\rtl_433_win\button3>..\rtl_433.exe -C si -r g018_433.92M_250k_cut.cu8.aiff -a
rtl_433 version unknown inputs file rtl_tcp RTL-SDR

Registered 104 out of 134 device decoding protocols [ 1-4 8 11-12 15-17 19-21 23 25-26 29-36 38-60 63 67-71 73-100 102-103 108-116 119 121 124-128 131-134 ]
Test mode active. Reading samples from file: g018_433.92M_250k_cut.cu8.aiff
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
time      : @0.452756s
model     : Smoke detector GS 558                  id        : 23118
unit      : 14           learn     : 0             Raw Code  : cb49ce
*** signal_start = -9964, signal_end = 158265, signal_len = 168229, pulses_found = 264
Iteration 1. t: 269    min: 138 (128)    max: 401 (136)    delta 2
Iteration 2. t: 269    min: 138 (128)    max: 401 (136)    delta 0
Pulse coding: Short pulse length 138 - Long pulse length 401

Short distance: 124, long distance: 385, packet distance: 1823

p_limit: 269
bitbuffer:: Number of rows: 8
[00] {33} 73 92 d3 93 00 : 01110011 10010010 11010011 10010011 0
[01] {33} 73 92 d3 93 00 : 01110011 10010010 11010011 10010011 0
[02] {33} 73 92 d3 93 00 : 01110011 10010010 11010011 10010011 0
[03] {33} 73 92 d3 93 00 : 01110011 10010010 11010011 10010011 0
[04] {33} 73 92 d3 93 00 : 01110011 10010010 11010011 10010011 0
[05] {33} 73 92 d3 93 00 : 01110011 10010010 11010011 10010011 0
[06] {33} 73 92 d3 93 00 : 01110011 10010010 11010011 10010011 0
[07] {33} 73 92 d3 93 00 : 01110011 10010010 11010011 10010011 0
Iteration 1. t: 0    min: 0 (0)    max: 0 (0)    delta -727379968
Iteration 2. t: 0    min: 0 (0)    max: 0 (0)    delta 0
Distance coding: Pulse length 0

Short distance: 1000000, long distance: 0, packet distance: 0

p_limit: 0
bitbuffer:: Number of rows: 0
```
